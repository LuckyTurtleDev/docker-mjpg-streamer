`docker-compose` example with two cams:
```yaml
version: "3.4"
services:
    mjpg-streamer-1:
       image: registry.gitlab.com/luckyturtledev/docker-mjpg-streamer:latest
       ports:
            - '8080:8080'
       command: -i "input_uvc.so -n -d /dev/video0 -r 1920x1080" -o "output_http.so -w  /usr/share/mjpg-streamer/www/"
       devices:
            - /dev/video0
       restart: unless-stopped

    mjpg-streamer-2:
       image: registry.gitlab.com/luckyturtledev/docker-mjpg-streamer:latest
       ports:
            - '8081:8080'
       command: -i "input_uvc.so -n -d /dev/video1 -r 1280x720" -o "output_http.so"
       devices:
            - /dev/video1
       restart: unless-stopped
```

The first container has also an optional web interface enabled, at this example.

With this configuration the streams are aviable at:
```
http://IP:8080/?action=stream
http://IP:8080/?action=snapshot
http://IP:8081/?action=stream
http://IP:8081/?action=snapshot
```
